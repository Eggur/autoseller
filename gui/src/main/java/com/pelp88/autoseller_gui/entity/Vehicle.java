package com.pelp88.autoseller_gui.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
public class Vehicle {
    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "label")
    private String label;

    @JsonProperty(value = "model")
    private String model;

    @JsonProperty(value = "category")
    private String category;

    @JsonProperty(value = "registrationId")
    private String registrationId;

    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "year")
    private Integer year;

    @JsonProperty(value = "sidecar")
    private Boolean sidecar;

    public boolean equals(com.pelp88.autoseller.entity.Vehicle anotherVehicle){
        if (!this.label.equals(anotherVehicle.getLabel())){
            return false;
        }

        if (!this.model.equals(anotherVehicle.getModel())){
            return false;
        }

        if (!this.category.equals(anotherVehicle.getCategory())){
            return false;
        }

        if (!this.registrationId.equals(anotherVehicle.getRegistrationId())){
            return false;
        }

        if (!this.type.equals(anotherVehicle.getType())){
            return false;
        }

        if (!this.year.equals(anotherVehicle.getYear())){
            return false;
        }

        if (!this.sidecar.equals(anotherVehicle.getSidecar())){
            return false;
        }

        return true;
    }
}
