package com.pelp88.autoseller_gui.entity;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VehicleRepository { // pseudo - repository :)
    private List<Vehicle> vehicleList = new ArrayList<>();
    public List<Vehicle> findAll(){
        return vehicleList;
    };

    public void saveAll(List<Vehicle> vehicle){
        for (var elem : vehicle){
            save(elem);
        }
    }

    public void save(Vehicle vehicle){
        var add = true;
        for (var elem : vehicleList){
            if (vehicle.getId().equals(elem.getId())){
                vehicleList.set(vehicleList.indexOf(elem), vehicle);
                add = false;
                break;
            }
        }

        if (add){
            vehicleList.add(vehicle);
        }
    }
}
