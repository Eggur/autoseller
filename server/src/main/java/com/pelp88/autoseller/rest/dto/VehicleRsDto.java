package com.pelp88.autoseller.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VehicleRsDto {
    @Schema(description = "ID автомобиля")
    private Long id;

    @Schema(description = "Марка автомобиля")
    private String label;

    @Schema(description = "Модель автомобиля")
    private String model;

    @Schema(description = "Категория регистрации ТС")
    private String category;

    @Schema(description = "Госномер регистрации ТС")
    private String registrationId;

    @Schema(description = "Тип ТС")
    private String type;

    @Schema(description = "Год выпуска ТС")
    private Integer year;

    @Schema(description = "Наличие прицепа (да/нет)")
    private Boolean sidecar;
}
