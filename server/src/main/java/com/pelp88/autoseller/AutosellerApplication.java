package com.pelp88.autoseller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutosellerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutosellerApplication.class, args);
    }
}
